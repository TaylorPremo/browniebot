import json

import discord
from discord.ext import commands


class Roles(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        # Opening the allowed roles file
        with open('data/roles.json') as data:
            self.allowed_roles = json.load(data)

    @commands.command()
    async def roles(self, ctx):
        """Lists all assignable roles on the server"""
        # Creates an array that holds all role names
        roles = []

        # Getting all roles, if they are allowed
        for role in ctx.guild.roles:
            if role.name in self.allowed_roles:
                roles.append(role.name)

        # Sorting the roles
        roles.sort()

        # Formatting the text
        list_of_roles = '```\n'
        for _, role in enumerate(roles):
            list_of_roles += role + '\n'
        list_of_roles += '```'
        await ctx.send(f'The assignable roles are:\n{list_of_roles}')

    @commands.command()
    async def roleinfo(self, ctx, *, message: str = None):
        """Displays information about the given role, if it is allowed"""
        if message:
            for role in ctx.guild.roles:
                # If the role exists and if they are allowed
                if role.name == message and message in self.allowed_roles:
                    # If at least 1 user has the role
                    if role.members:
                        role_information = f'**{role.name}**\n```\n'

                        for member in role.members:
                            # Since the message character limit is 2000, splits the message
                            # into ones within the limit Makes sure to leave space for the ```
                            if len(role_information) + len(str(member)) > 1997:
                                await ctx.send(f'{role_information}```')
                                role_information = '```'
                            role_information += f'{str(member)}\n'
                        await ctx.send(f'{role_information}```')
                    else:
                        await ctx.send(f'No user has the role "{role.name}"')
                    return
            clean_message = await self.bot.clean_converter.convert(ctx, message)
            await ctx.send(f'Role "{clean_message}" does not exist, or it is not allowed')
        else:
            await ctx.send(f'```.roleinfo [role]\n\nDisplays information '
                           f'about the given role, if it is allowed.```')

    @commands.command()
    async def createrole(self, ctx, *, message: str = None):
        """Creates a role with the given name"""
        # Checks if the user is allowed to call this command
        if self.bot.is_valid(ctx.author.id):
            if message:
                # Checks if a role with the given name already exists
                if not any(role.name == message for role in ctx.guild.roles):
                    # Creates the role
                    await ctx.guild.create_role(
                        name=message,
                        permissions=discord.Permissions(0),
                        colour=discord.Colour(0),
                        hoist=False,
                        mentionable=False
                    )
                    self.allowed_roles.append(message)

                    with open('data/roles.json', 'w') as data:
                        json.dump(self.allowed_roles, data, ensure_ascii=False)

                    await ctx.send(f'Role "{message}" successfully created')
                else:
                    await ctx.send(f'Role "{message}" already exists')
            else:
                await ctx.send(f'```.createrole [roleName]\n\nCreates a role '
                               f'with the given name.```')

    @commands.command()
    async def deleterole(self, ctx, *, message: str = None):
        """Deletes the role with the given name, if it is allowed"""
        # Checks if the user is allowed to call this command
        if self.bot.is_valid(ctx.author.id):
            if message:
                # Checks if a role with the given name exists, and it is allowed
                for role in ctx.guild.roles:
                    if role.name == message and message in self.allowed_roles:
                        # Deletes the role
                        await role.delete()
                        self.allowed_roles.remove(message)

                        with open('data/roles.json', 'w') as data:
                            json.dump(self.allowed_roles, data,
                                      ensure_ascii=False)

                        await ctx.send(f'Role "{message}" successfully deleted')
                        return
                await ctx.send(f'Role "{message} does not exist, or it is not allowed')
            else:
                await ctx.send(f'```.deleterole [roleName]\n\nDeletes the role '
                               f'with the given name, if it is allowed.```')

    @commands.command()
    async def addrole(self, ctx, *, message: str = None):
        """Adds the given role to the allowed roles"""
        if self.bot.is_valid(ctx.author.id):
            if message:
                # Checks if a role with the given name exists
                for role in ctx.guild.roles:
                    # Checks if a role with the given name exists
                    if role.name == message:
                        if message in self.allowed_roles:
                            await ctx.send(f'Role "{message}" is already allowed')
                            return

                        self.allowed_roles.append(message)

                        with open('data/roles.json', 'w') as data:
                            json.dump(self.allowed_roles, data,
                                      ensure_ascii=False)

                        await ctx.send(f'Role "{message}" successfully added to allowed roles')
                        return
                await ctx.send(f'Role "{message}" does not exist')
            else:
                await ctx.send(f'```.addrole [roleName]\n\nAdds the given role '
                               f'to the allowed roles.```')

    @commands.command()
    async def removerole(self, ctx, *, message: str = None):
        """Removes the given role from the allowed roles"""
        if self.bot.is_valid(ctx.author.id):
            if message:
                # Checks if a role with the given name exists
                for role in ctx.guild.roles:
                    # Checks if a role with the given name exists
                    if role.name == message:
                        if message in self.allowed_roles:
                            self.allowed_roles.remove(message)

                            with open('data/roles.json', 'w') as data:
                                json.dump(self.allowed_roles, data,
                                          ensure_ascii=False)

                            await ctx.send(f'Role "{message}" successfully removed from allowed roles')
                            return

                        await ctx.send(f'Role "{message}" is not allowed')
                        return
                await ctx.send(f'Role "{message}" does not exist')
            else:
                await ctx.send(f'```.removerole [roleName]\n\nRemoves the given role '
                               f'from the allowed roles.```')

    @commands.command()
    async def iam(self, ctx, *, message: str = None):
        """Assigns the user the given role, if it is allowed"""
        if message:
            # Checks if a role with the given name exists
            for role in ctx.guild.roles:
                # Checks if a role with the given name exists, and it is allowed
                if role.name == message and message in self.allowed_roles:
                    # Checks if the user already has that role
                    if role not in ctx.author.roles:
                        try:
                            await ctx.author.add_roles(role)
                            await ctx.send(f'I\'ve given you the "{message}" role')
                        except Exception as e:
                            print(e)
                            await ctx.send(f'I can\'t assign the "{message}" role')

                    else:
                        await ctx.send(f'You already have the "{message}" role')
                    return
            clean_message = await self.bot.clean_converter.convert(ctx, message)
            await ctx.send(f'Role "{clean_message}" does not exist, or it is not allowed')
        else:
            await ctx.send(f'```.iam [roleName]\n\nAssigns the user '
                           f'the given role, if it is allowed.```')

    @commands.command()
    async def iamnot(self, ctx, *, message: str = None):
        """Removes the user the given role, if it is allowed"""
        if message:
            # Checks if a role with the given name exists
            for role in ctx.guild.roles:
                # Checks if a role with the given name exists, and it is allowed
                if role.name == message and message in self.allowed_roles:
                    # Checks if the user already has that role
                    if role in ctx.author.roles:
                        try:
                            await ctx.author.remove_roles(role)

                            await ctx.send(f'I\'ve removed the "{message}" role from you')
                        except Exception as e:
                            print(e)
                            await ctx.send(f'I can\'t remove the "{message}" role')
                    else:
                        await ctx.send(f'You don\'t have the "{message}" role')
                    return
            clean_message = await self.bot.clean_converter.convert(ctx, message)
            await ctx.send(f'Role "{clean_message}" does not exist, or it is not allowed')
        else:
            await ctx.send(f'```.iamnot [roleName]\n\nRemoves the given role '
                           f'from the user, if it is allowed.```')

    @commands.command()
    async def mention(self, ctx, *, message: str = None):
        """Makes the role mentionable if it isn't, and then mentions it"""
        # Checks if the user is allowed to call this command
        if self.bot.is_valid(ctx.author.id):
            if message:
                # Checks if a role with the given name exists
                for role in ctx.guild.roles:
                    # Checks if a role with the given name exists, and it is allowed
                    if role.name == message and message in self.allowed_roles:
                        # Gets the mentionable status of the role
                        mention_status = role.mentionable
                        try:
                            # Makes the role mentionable
                            if not mention_status:
                                await role.edit(mentionable=True)

                            # Mentions the role
                            await ctx.send(role.mention)

                            # Makes the role not mentionable
                            if not mention_status:
                                await role.edit(mentionable=mention_status)
                        except Exception as e:
                            await ctx.send(e)
                            await ctx.send(f'I can\'t mention the {message} role')
                        return
                await ctx.send(f'Role "{message}" does not exist, or it is not allowed')
            else:
                await ctx.send(f'```.mention [roleName]\n\nMentions the given role '
                               f', if it is allowed```')


def setup(bot):
    bot.add_cog(Roles(bot))
