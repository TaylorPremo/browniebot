from discord.ext import commands


async def userIsValid(ctx):
    return ctx.bot.isValid(ctx.author.id)


class Mod(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    # TODO: replace with @commands.has_permissions(discord.Permissions.manage_messages)
    def can_delete(self, ctx):
        """Checks if the user can delete messages"""
        return ctx.author.permissions_in(ctx.channel).manage_messages

    # TODO: replace with @commands.has_permissions(discord.Permissions.ban_members)
    def can_ban(self, ctx):
        """Checks if the user can ban users"""
        return ctx.author.permissions_in(ctx.channel).ban_members

    async def botLoad(self, ctx, extensionName: str):
        """Loads an extension"""
        try:
            self.bot.load_extension(f'cogs.{extensionName}')
        except Exception as error:
            await ctx.send(f'{type(error).__name__}\n{error}')
            return
        await ctx.send(f'{extensionName} loaded')

    async def botUnload(self, ctx, extensionName: str):
        """Unloads an extension"""
        try:
            self.bot.unload_extension(f'cogs.{extensionName}')
        except Exception as error:
            await ctx.send(f'{type(error).__name__}\n{error}')
            return
        await ctx.send(f'{extensionName} unloaded')

    async def botReload(self, ctx, extensionName: str):
        """Reloads an extension"""
        await self.botUnload(ctx, extensionName)
        await self.botLoad(ctx, extensionName)

    @commands.command()
    @commands.check(userIsValid)
    async def load(self, ctx, extensionName: str = None):
        """Command wrapper for botLoad"""
        # Checks if the user is allowed to call this command
        if self.bot.isValid(ctx.author.id):
            # Checks if something was passed in
            if extensionName:
                await self.botLoad(ctx, extensionName)

    @commands.command()
    async def unload(self, ctx, extensionName: str = None):
        """Command wrapper for botUnload"""
        # Checks if the user is allowed to call this command
        if self.bot.isValid(ctx.author.id):
            # Checks if something was passed in
            if extensionName:
                # Makes it so you can not only unload this cog
                if (extensionName == 'mod'):
                    await ctx.send('Please do not unload this')
                    return
                await self.botUnload(ctx, extensionName)

    @commands.command()
    async def reload(self, ctx, extensionName: str = None):
        """Command wrapper for botReload"""
        # Checks if the user is allowed to call this command
        if self.bot.isValid(ctx.author.id):
            # Checks if something was passed in
            if extensionName:
                await self.botReload(ctx, extensionName)

    @commands.command()
    async def delete(self, ctx, numberOfMessages: str = None):
        """Deletes [x] messages from the channel this command was typed in (Can only be 1-99)"""
        if self.canDelete(ctx):
            # Checks to make sure a value was passed in
            if numberOfMessages:
                deletedMessages = []
            else:
                await ctx.send(f'```.delete [number]\n\nType a number, '
                               f'and delete that many messages, including '
                               f'the call to it. (Only accepts 1-99)```')
                return

            # Checks if a number was typed
            try:
                numberOfMessages = int(numberOfMessages) + 1
            except ValueError:
                await ctx.send(f'Please type a number')
                return

            # Checks if the number is between 1 and 99
            if ((numberOfMessages < 2) or (numberOfMessages > 100)):
                await ctx.send(f'Please make sure your number is '
                               f'between 1 and 99')
                return

            # Deletes the messages
            async for message in ctx.channel.history(limit=numberOfMessages):
                deletedMessages.append(message)
            await ctx.channel.delete_messages(deletedMessages)

    @commands.command()
    async def ban(self, ctx, username: str = None, *, reason: str = None):
        """Bans the given user"""
        # Checks if the user can ban another user
        if (self.canBan(ctx)):
            # Getting the user
            user = self.bot.getUser(ctx, username)

            # If the user is not on the server
            if not user:
                user = await self.bot.fetch_user(username)

            # Checks if a user was found
            if user:
                # Making sure that the user does not ban themself
                if user == ctx.author:
                    await ctx.send("Don't ban yourself!!! >.<")
                else:
                    # Checks if the user you want to ban is valid
                    # Work on a better way to decide this
                    if self.bot.isValid(user.id):
                        await ctx.send("Don't ban other cool people!!! >.<")
                    else:
                        # Bans the given user
                        await ctx.send(f'{username}, get bapped nerd!')
                        await ctx.guild.ban(user, reason=reason, delete_message_days=0)
            else:
                await ctx.send(f'User {username} was not found')

    @commands.command()
    async def shutdown(self, ctx):
        """Shuts down the bot"""
        # Checks if the user is allowed to call this command
        if self.bot.isValid(ctx.author.id):
            await ctx.send(f'Bai bai :wave:')
            await self.bot.logout()


def setup(bot):
    bot.add_cog(Mod(bot))
