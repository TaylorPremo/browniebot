import asyncio
import json

import discord
from discord.ext import commands


class Reactions(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        with open('data/reactions.json') as data:
            self.reaction_data = json.load(data)

        self.update_reaction_data_listener = self.bot.loop.create_task(
            self.update_data())

    def __exit__(self, exc_type, exc_value, traceback):
        """To cancel any running tasks"""
        self.update_reaction_data_listener.cancel()

    async def update_data(self):
        """Writing the reaction data every 5 minutes"""
        while True:
            await asyncio.sleep(5*60)
            with open('data/reactions.json', 'w') as data:
                json.dump(self.reaction_data, data, ensure_ascii=False)

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        """Watching reactions by users"""
        # Cause bots aren't people
        if not user.bot:
            # Cause JSON won't let me store ints as keys
            user_id = str(user.id)
            author_id = str(reaction.message.author.id)

            # If the person reacting does not have an entry
            if user_id not in self.reaction_data:
                self.reaction_data[user_id] = {}
                self.reaction_data[user_id]["from"] = {}
                self.reaction_data[user_id]["to"] = {}

            # If the person being reacted to does not have an entry
            if author_id not in self.reaction_data:
                self.reaction_data[author_id] = {}
                self.reaction_data[author_id]["from"] = {}
                self.reaction_data[author_id]["to"] = {}

            # If the emoji is a custom emoji
            if isinstance(reaction.emoji, (discord.Emoji, discord.PartialEmoji)):
                check_reaction = ':' + reaction.emoji.name + ':'
                if check_reaction not in self.reaction_data[user_id]["from"]:
                    self.reaction_data[user_id]["from"][check_reaction] = 0
                self.reaction_data[user_id]["from"][check_reaction] += 1
                if check_reaction not in self.reaction_data[author_id]["to"]:
                    self.reaction_data[author_id]["to"][check_reaction] = 0
                self.reaction_data[author_id]["to"][check_reaction] += 1
            else:
                if reaction.emoji not in self.reaction_data[user_id]["from"]:
                    self.reaction_data[user_id]["from"][reaction.emoji] = 0
                self.reaction_data[user_id]["from"][reaction.emoji] += 1
                if reaction.emoji not in self.reaction_data[author_id]["to"]:
                    self.reaction_data[author_id]["to"][reaction.emoji] = 0
                self.reaction_data[author_id]["to"][reaction.emoji] += 1

    @commands.Cog.listener()
    async def on_reaction_remove(self, reaction, user):
        """Watching reactions by users"""
        # Cause bots aren't people
        if not user.bot:
            # Cause JSON won't let me store ints as keys
            user_id = str(user.id)
            author_id = str(reaction.message.author.id)

            # If the person reacting does not have an entry
            if user_id not in self.reaction_data:
                self.reaction_data[user_id] = {}
                self.reaction_data[user_id]["from"] = {}
                self.reaction_data[user_id]["to"] = {}

            # If the person being reacted to does not have an entry
            if author_id not in self.reaction_data:
                self.reaction_data[author_id] = {}
                self.reaction_data[author_id]["from"] = {}
                self.reaction_data[author_id]["to"] = {}

            # If the emoji is a custom emoji
            if isinstance(reaction.emoji, (discord.Emoji, discord.PartialEmoji)):
                check_reaction = ':' + reaction.emoji.name + ':'
                if check_reaction not in self.reaction_data[user_id]["from"]:
                    self.reaction_data[user_id]["from"][check_reaction] = 0
                self.reaction_data[user_id]["from"][check_reaction] -= 1
                if check_reaction not in self.reaction_data[author_id]["to"]:
                    self.reaction_data[author_id]["to"][check_reaction] = 0
                self.reaction_data[author_id]["to"][check_reaction] -= 1
            else:
                if reaction.emoji not in self.reaction_data[user_id]["from"]:
                    self.reaction_data[user_id]["from"][reaction.emoji] = 0
                self.reaction_data[user_id]["from"][reaction.emoji] -= 1
                if reaction.emoji not in self.reaction_data[author_id]["to"]:
                    self.reaction_data[author_id]["to"][reaction.emoji] = 0
                self.reaction_data[author_id]["to"][reaction.emoji] -= 1

    @commands.command()
    async def mystats(self, ctx):
        """Outputs the user's top reactions"""
        user = ctx.author
        data = discord.Embed(colour=user.colour)
        data.set_author(name=str(user))
        data.set_thumbnail(url=user.avatar_url)

        # Cause JSON won't let me store ints as keys
        user_id = str(user.id)

        # Getting the user's top reactions
        received_reactions = self.reaction_data[user_id]["to"]
        sorted_received_reactions = sorted(
            received_reactions.items(), key=lambda reaction: reaction[1], reverse=True)

        reaction_counter = 0
        received_reactions_data = ""
        for emoji_data in sorted_received_reactions:
            received_reactions_data += f"{emoji_data[0]} - {emoji_data[1]}"
            reaction_counter += 1

            if reaction_counter == 10:
                break

            received_reactions_data += ", "

        data.add_field(
            name='Top 10 received reactions:',
            value=(received_reactions_data),
            inline=False)

        # Getting the user's most received reactions
        given_reactions = self.reaction_data[user_id]["from"]
        sorted_given_reactions = sorted(
            given_reactions.items(), key=lambda reaction: reaction[1], reverse=True)

        reaction_counter = 0
        given_reactions_data = ""
        for emoji_data in sorted_given_reactions:
            given_reactions_data += f"{emoji_data[0]} - {emoji_data[1]}"
            reaction_counter += 1

            if reaction_counter == 10:
                break

            given_reactions_data += ", "

        data.add_field(
            name='Top 10 given reactions:',
            value=(given_reactions_data),
            inline=False)

        # Printing them
        await ctx.send(embed=data)


def setup(bot):
    bot.add_cog(Reactions(bot))
