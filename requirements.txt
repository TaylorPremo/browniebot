-i https://pypi.org/simple
aiohttp==3.5.4
async-timeout==3.0.1
attrs==19.1.0
chardet==3.0.4
discord-py==1.2.3
idna-ssl==1.1.0 ; python_version < '3.7'
idna==2.8
multidict==4.5.2
pyfiglet==0.8.post1
pyyaml==5.1.2
typing-extensions==3.7.4 ; python_version < '3.7'
websockets==6.0
yarl==1.3.0
youtube-dl==2019.9.1
wolframalpha==4.0.0